=======================================
WakatobiAIS (BAKTI) Bluetooth Interface
=======================================

Overview
========

**Note to readers (1): this page only applies to WakatobiAIS (BAKTI)**

**Note to readers (2): The frame structures and communication protocols explained below are not final. Critics and improvement suggestions are very welcomed!**

BAKTI mobile application requires the AIS equipment to have three functionality:

1. Tracking
2. Tagging
3. Search and Rescue / Emergency

**Tracking functionality** requires the AIS equipment to periodically transmit position information along with other additional information such as ship's name, MMSI number, and call sign. Additionally, this functionality requires the AIS equipment to provide position information (latitude and longitude) to a Bluetooth connected BAKTI mobile application when requested. **Tagging functionality** requires the AIS equipment to be capable of receiving encrypted tagging information from the Bluetooth connected BAKTI mobile application at all times. This encrypted tagging information will then be transmitted by the AIS equipment through AIS message 8 (binary broadcast message). **Search and rescue / Emergency functionality** requires the AIS equipment to be capable of being switched into AIS-SART (AIS Search and Rescue Transponder) at all times (preferably at emergency situations), sending bursts of position information and emergency related broadcast message every minute. Content of the emergency related broadcast message should be able to be chosen through the Bluetooth connected BAKTI mobile application. Moreover, switching into AIS-SART mode should be able to be triggered through the BAKTI mobile application.

Therefore, a standard frame structure and simple communication protocol has been developed for the communication between WakatobiAIS and BAKTI mobile application through a Bluetooth interface.

**Important: if the reader wishes to bypass all high level functionality dercribed below (Tracking Functionality, Tagging Functionality, and Emergency Functionality), and prefers to send raw AIS messages for manual transmissions, readers are adviced to read the last section titled "Manual Transmission".**

Bluetooth Low Energy (BLE) Protocol
===================================

From all the Bluetooth versions available, we have chosen BLE because of its low power usage that is of paramount importance for the AIS equipment. BLE have what is called **services** and **characteristics**. Basically they are information container that can either be read, written, or read and written. For example, the generic access service (:code:`0x1800`) have a device name characteristic (:code:`0x2A00`) that can be read.

The Bluetooth module used in WakatobiAIS (HM-10 module with CC2540/CC2541 TI BLE chip) also has a custom service (:code:`0xFFE0`) with a custom characteristic (:code:`0xFFE1`) that can be read, written, and is notifiable (The connected device is notified when the value changed). The communication between WakatobiAIS and BAKTI mobile application utilizes this custom service/characteristic. Sending a message, either from WakatobiAIS or BAKTI mobile application, is done by writing the message to the custom characteristic value while receiving a message, either from WakatobiAIS or BAKTI mobile application, is done by reading the message from the custom characteristic value.

However, the reader must be aware that a BLE characteristic value is only 20 octets wide. Therefore messages that are wider then 20 octets wide must be segmented in to 20 octets wide chunks and transmitted (written to the custom characteristics field) sequentially.

Message Frame Structure
=======================

.. image:: images/BT-frame-structure.png
   :align: center
   :width: 600

The start of frame byte signifies the beginning of a valid message. This is important for the receiver to resynchronize to a start of a valid message when errors exist in previous messages. Message classification byte is required by the receiver to know what information is contained in the payload. For example, a message classification byte of :code:`A (0x41)` indicates that the payload is a string of characters that will be transmitted inside the emergency broadcast message. This will be explained more thoroughly for each message class later. The end of frame byte signifies the end of the payload segment and may be important if the payload size is allowed to be variable for certain messages. Moreover, this redundancy decreases the chance of encountering false positives. The last two byte is the checksum of the message written in 2 ASCII character corresponding to the checksum value written in uppercase hexadecimal value.

Tracking Functionality
======================

To request the latitude and longitude value from WakatobiAIS, the following message must be sent:

.. image:: images/GPS-request-frame-structure.png
   :align: center
   :width: 250

After WakatobiAIS received the above message while however **the GPS receiver on WakatobiAIS has not yet locked on to a location**, it will respond with the following message:

.. image:: images/void-GPS-respond-frame-structure.png
   :align: center
   :width: 550

However, **if the GPS receiver has already locked on to a location**, it will respond with the following message:

.. image:: images/locked-GPS-respond-frame-structure.png
   :align: center
   :width: 600

:code:`Dl1 Dl2` are the two digits of latitude's degree value, while :code:`Ml1 Ml2 Ml3 Ml4` are the four digits of the latitude's minute value in 1/100th of a minute. :code:`'N' / 'S'` referes to the latitude's direction, either north or south. For example, if :code:`'Dl1' = '0'`, :code:`'Dl2' = '6'`, :code:`'Ml1' = '2'`, :code:`'Ml2' = '3'`, :code:`'Ml3' = '1'`, :code:`'Ml4' = '6'`, and the direction is :code:`'N'`, the overall latitude value is 6'23.16"N. Interpretation of the longitude fields follows the same exact rule, the only difference is the additional hundreds position for the degree value.

The following algorithm written in Python can be used as a reference:

.. code-block:: python
   
   # Latitude
   # char array Dl1, Dl2, Ml1, Ml2, Ml3, Ml4, Direction ('N' or 'S')
   charArray = ['0','6','2','3','1','6','N']
   latitude_degree = (ord(charArray[0])-ord('0'))*10 + (ord(charArray[1])-ord('0')) # 6 degree
   latitude_minute = (ord(charArray[2])-ord('0'))*10 + (ord(charArray[3])-ord('0')) + (ord(charArray[4])-ord('0'))/10 + (ord(charArray[5])-    ord('0'))/100 # 23.16 minute
   latitude_degree_total = latitude_degree + latitude_minute/60 # 6.386 degree north
   # RESULT : Latitude = 6 degrees 23.16 minutes north = 6.386 degrees north

   # Longitude
   # char array Dy1, Dy2, Dy3, My1, My2, My3, My4, Direction ('E' or 'W')
   charArray = ['1','0','6','5','4','2','6','E']
   longitude_degree = (ord(charArray[0])-ord('0'))*100 + (ord(charArray[1])-ord('0'))*10 + (ord(charArray[2])-ord('0')) # 106 degree
   longitude_minute = (ord(charArray[3])-ord('0'))*10 + (ord(charArray[4])-ord('0')) + (ord(charArray[5])-ord('0'))/10 + (ord(charArray[6])-  ord('0'))/100 # 54.26 minute
   longitude_degree_total = longitude_degree + longitude_minute/60 # 106.904 degree east
   # RESULT : Longitude = 106 degrees 54.26 minutes east = 106.904 degrees east


Search and Rescue / Emergency Functionality
===========================================

To switch WakatobiAIS in to AIS-SART mode, the following message must be sent:

.. image:: images/message-alert-request-frame-structure.png
   :align: center
   :width: 500

The payload should be filled with 16 characters that would be sent inside the emergency-related broadcast message. If the message is shorter than 16 characters, it should be filled with spaces (ASCII 32). Only ASCII characters listed in the table below (with the exeption of :code:`'!'` and :code:`'*'`) are allowed inside the payload.

.. image:: images/ascii-table.png
   :align: center
   :width: 200

In addition to missing start and end of frame byte (:code:`'!'` and :code:`'*'`) and mismatched checksum, failure to comply with the constraint mentioned above will result in ignored/dropped request. If the message was not ignored/dropped, WakatobiAIS will respond with the following confirmation message:

.. image:: images/good-alert-respond-frame-structure.png
   :align: center
   :width: 300

A timeout of around 500 ms should be employed in the mobile application side while waiting for the above response. If the timeout is exceeded, the request should be repeated until the response above is acquired. After the request is confirmed, there is a 10 s timeout before the actual switching to AIS-SART mode truly begins. In this time frame, the user can request to cancel the switching action by sending the following message:

.. image:: images/cancel-alert-request-frame-structure.png
   :align: center
   :width: 300

If the cancel request is accepted, WakatobiAIS will respond with the same message as the cancel request above to the mobile application. As before, timeout mechanism should be employed to make sure the cancel request will be executed.

If no cancel request sent within the 10 s timeout, the device will be switched into AIS-SART mode, and it cannot be switched back to normal mode. The device will transmit AIS message 14 (containing the sentence sent) and AIS message 18 periodically.

Tagging Functionality
=====================

Two information that must be supplied by the mobile application for tagging purposes are commodity category and commodity weight. This two information should be sent through the bluetooth interface according to this frame structure:

.. image:: images/tagging-request-frame-structure.png
   :align: center
   :width: 600

Commodity category is a 1 to 10 digit number represented in ASCII characters from :code:`"0"` to :code:`"4294967295"`. Each different number corresponds to a different commodity. For example, the number :code:`"2"` may corresponds to a pacific bluefin tuna while :code:`"15"` may corresponds to a european pilchard. Commodity weight is a 1 to 10 digit number represented in ASCII characters from :code:`"0"` to :code:`"4294967295"` that corresponds to the catched commodity weight in kilograms.

As in the emergency functionality, the following confirmation message will be sent by the AIS equipment if the tagging request is received correctly.

.. image:: images/tagging-respond-frame-structure.png
   :align: center
   :width: 300

The payload will then be encrypted by the AIS equipment before broadcasted through air to prevent unintended receivers from interpreting the message.

Checksum Calculation
====================

The checksum calculation method employed is the parity word checksum. To calculate the checksum value, simply XOR every byte in the message (including :code:`!` and :code:`*`). The acquired 8-bit checksum value hexadecimal representation in 2 ASCII characters are then appended to the end of the message. For example, if the checksum value is :code:`0xA5`, then the last two characters of the message should be :code:`A` and :code:`5`. The following algorithm written in Python can be used as a reference:

.. code-block:: python
   
   def bitToHexChar(x):
      # Convert a 4 bit value to its hexadecimal representation in ASCII
      if(x < 10):
         return chr(x + ord('0'))
      else:
         return chr(x - 10 + ord('A'))
   
   # Switch to AIS-SART mode request
   characters = ['!','A','E','M','P','T','Y',' ','F','U','E','L',' ',' ',' ',' ',' ',' ','*','','']
   checksum = 0
   for i in range(len(characters)-2):
      checksum = checksum ^ ord(characters[i])
   characters[len(characters)-2] = bitToHexChar(checksum >> 4)
   characters[len(characters)-1] = bitToHexChar(checksum & 0x0f)

   # Result : characters = ['!','A','E','M','P','T','Y',' ','F','U','E','L',' ',' ',' ',' ',' ',' ','*','2','5']

Manual Transmission
===================

As an alternative to all the above methods, the bluetooth application could instead send an AIVDM/AIVDO sentence containing the AIS message to transmit manually. This method requires the bluetooth application to handle all higher level functionality such as ensuring only 1 slot transmission will occur (e.g. Maximum payload size of 168 bits) and transmission scheduling (e.g. when to transmit message 14, 18, etc.), because there will be no checks implemented inside the device and it will transmit the message right away.

