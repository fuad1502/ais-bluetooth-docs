.. AIS Bluetooth documentation master file, created by
   sphinx-quickstart on Wed Jan 23 10:48:37 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========================================
Welcome to AIS Bluetooth's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Tabel of Contents:
   
   bluetooth
   
Project Description
===================
*Blank*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
